package UD20_ex1.UD20_ex1App;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class UD20_ex1Frame extends JFrame {

	private JPanel contentPane;
	private JPanel contentPane_1;

	//Aquí ejecuto el frame creado posteriormente
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UD20_ex1Frame frame = new UD20_ex1Frame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private JLabel lblNewLabel;
	
	public UD20_ex1Frame() {
		//Aquí indico las configuraciones principales de mi ventana, como su tamaño, bordes, relleno
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setVisible(true);
		//Aquí le doy un valor al panel en la ventana
		contentPane_1 = new JPanel();
		contentPane_1.setLayout(null);
		setContentPane(contentPane_1);
		//Aquí creo una JLabel llamada etiqueta
		lblNewLabel = new JLabel("Etiqueta");
		//Aquí creo una accion que cuando el usuario haga click en el texto de la etiqueta, su tamaño cambie
		lblNewLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				//La etiqueta cambiará de fuente y de tamaño tal como he especificado aquí
				lblNewLabel.setFont(new Font("Serif", Font.BOLD, 25));
			}
		});
		lblNewLabel.setBounds(180, 30, 113, 44);
		//Aquí lo añado al panel
		contentPane_1.add(lblNewLabel);
	}
}
